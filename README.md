# flutter_app

A Practical Task for edexo

## Completed task:
1.Create a UI for creating a folder. (Prefer screenshot:- MyFolder-ui.jpeg)

    a.	Parent folder
        -	Subfolder
        -	Create N number of folders

    b. View Type (Prefer screenshot: Multi-selection and list view ui.jpeg)
        -	List View
        -	Grid View

2.Create logic for multi-selection folders. (Prefer screenshot  Multi-selection and list view ui.jpeg)

    -	Show selection counts.
    -	Show only delete icon(no functionalities)
    -	Should be in the parent folder, subfolder to n number of folders.

Takes 3 hours of development time to complete above task.

not use of any database.. only temporary initialization of folder is created. after kill the application the all created folders will remove from the application.
