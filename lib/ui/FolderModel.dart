import 'dart:core';

class FolderModel {
  static int objCounter = 0;
  int id;
  int parentId;
  String name;
  List<FolderModel> subFolderList;
  bool isSelected;
  bool isFile;
  int size;

  FolderModel(this.parentId, this.name, this.isFile,
      {bool isSelected = false, size = 0}) {
    this.isSelected = isSelected;
    this.size = size;
    subFolderList = List<FolderModel>();
    objCounter += 1;
    this.id = objCounter;
  }
}
