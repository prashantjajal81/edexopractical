import 'package:file_picker/file_picker.dart';
import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/ui/FolderModel.dart';
import 'package:flutter_app/ui/map_draw_path.dart';

class MyFolderUI extends StatefulWidget {
  final String title;
  final FolderModel parent;

  MyFolderUI({Key key, this.title, this.parent}) : super(key: key);

  @override
  _MyFolderUIState createState() => _MyFolderUIState();
}

class _MyFolderUIState extends State<MyFolderUI>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  List<FolderModel> folders;
  TextEditingController _searchFilterController;
  bool multiSelection = false;

  Animation<double> _animation;
  AnimationController _animationController;

  // bool selecction
  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);

    WidgetsBinding.instance.addObserver(this);
    if (widget.parent == null)
      folders = List<FolderModel>();
    else {
      folders = widget.parent.subFolderList;
    }
    _searchFilterController = TextEditingController();
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      setState(() {
        folders = folders;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: multiSelection
              ? IconButton(
                  icon: Icon(Icons.cancel, color: Colors.black),
                  onPressed: () {
                    removeSelection();
                    setState(() {
                      multiSelection = false;
                    });
                  },
                )
              : (widget.parent != null
                  ? IconButton(
                      icon: Icon(Icons.arrow_back, color: Colors.black),
                      onPressed: () => Navigator.of(context).pop(true),
                    )
                  : null),
          title: Text(multiSelection ? _getSelectedCount() : widget.title),
          actions: multiSelection
              ? [
                  IconButton(
                    icon: Icon(Icons.delete, color: Colors.black),
                    onPressed: () {},
                  )
                ]
              : null,
        ),
        body: Container(
            padding: EdgeInsets.only(top: 8, bottom: 8),
            child: ListView.builder(
              padding: EdgeInsets.only(left: 8, right: 8),
              itemCount: folders.length,
              itemBuilder: (context, index) => Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  elevation: 4,
                  child: InkWell(
                    onLongPress: () {
                      setState(() {
                        multiSelection = true;
                        folders[index].isSelected = !folders[index].isSelected;
                        setState(() {});
                      });
                    },
                    onTap: () async {
                      if (multiSelection) {
                        folders[index].isSelected = !folders[index].isSelected;
                        setState(() {});
                      } else if (folders[index].isFile) {
                        // disable click for  file
                      } else {
                        var result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MyFolderUI(
                                      title: "Sub folder list",
                                      parent: folders[index],
                                    )));
                        if (result) {
                          setState(() {});
                        }
                      }
                    },
                    child: Container(
                      color: folders[index].isSelected
                          ? Colors.amber
                          : Colors.white,
                      padding: EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            folders[index].name,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          Text(
                            folders[index].subFolderList.length > 0
                                ? getFileFolderCount(folders[index])
                                : folders[index].isFile
                                    ? (folders[index].size ~/ 1024).toString() +
                                        " KB"
                                    : "Empty folder",
                            style: Theme.of(context).textTheme.subtitle1,
                          )
                        ],
                      ),
                    ),
                  )),
            )),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActionBubble(
          // Menu items
          items: <Bubble>[
            // Floating action menu item
            Bubble(
              title: "Draw static path",
              iconColor: Colors.black,
              bubbleColor: Colors.yellow,
              icon: Icons.directions,
              titleStyle: TextStyle(fontSize: 16, color: Colors.black),
              onPress: () {
                _animationController.reverse();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            MapPathDraw(title: "Draw path on map")));
              },
            ),
            Bubble(
              title: "Folder",
              iconColor: Colors.black,
              bubbleColor: Colors.yellow,
              icon: Icons.folder,
              titleStyle: TextStyle(fontSize: 16, color: Colors.black),
              onPress: () {
                _animationController.reverse();
                _showDialog();
              },
            ),
            // Floating action menu item
            Bubble(
              title: "File",
              iconColor: Colors.black,
              bubbleColor: Colors.yellow,
              icon: Icons.file_upload,
              titleStyle: TextStyle(fontSize: 16, color: Colors.black),
              onPress: () async {
                _animationController.reverse();
                FilePickerResult result = await FilePicker.platform.pickFiles();

                if (result != null) {
                  print(result.files.single.name);
                  print(result.files.single.path);
                  print(result.files.single.size);
                  _addFile(result.files.single);
                  // File file = File();
                } else {
                  // User canceled the picker
                }
              },
            ),
          ],

          // animation controller
          animation: _animation,

          // On pressed change animation state
          onPress: _animationController.isCompleted
              ? _animationController.reverse
              : _animationController.forward,

          // Floating Action button Icon color
          // iconColor: Colors.blue,

          // Flaoting Action button Icon
          icon: AnimatedIcons.add_event,
        )
        /*FloatingActionButton(
        onPressed: _showDialog,
        tooltip: 'Add Folder',
        child: Icon(Icons.add),
      ),*/ // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  _showDialog() async {
    await showDialog<String>(
      context: context,
      child: new AlertDialog(
        contentPadding: const EdgeInsets.only(left: 16.0, right: 16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                controller: _searchFilterController,
                autofocus: true,
                decoration: new InputDecoration(labelText: 'Folder name'),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(child: const Text('ADD'), onPressed: _addFolder)
        ],
      ),
    );
  }

  void _addFolder() {
    if (widget.parent == null) {
      folders.add(FolderModel(-1, _searchFilterController.text, false));
    } else {
      folders.add(
          FolderModel(widget.parent.id, _searchFilterController.text, false));
    }
    setState(() {
      folders = folders;
    });
    _searchFilterController.text = "";
    Navigator.pop(context);
  }

  void _addFile(file) {
    if (widget.parent == null) {
      print(file.size);
      folders.add(FolderModel(-1, file.name, true, size: file.size));
    } else {
      print(file.size);
      folders
          .add(FolderModel(widget.parent.id, file.name, true, size: file.size));
    }
    setState(() {
      folders = folders;
    });
  }

  String _getSelectedCount() {
    int count = 0;
    folders.forEach((element) {
      if (element.isSelected) count++;
    });
    return "Selected " + count.toString();
  }

  void removeSelection() {
    folders.forEach((element) {
      element.isSelected = false;
    });
  }

  String getFileFolderCount(FolderModel folder) {
    var totalFolder =
        folder.subFolderList.where((element) => !element.isFile).length;
    var totalFile =
        folder.subFolderList.where((element) => element.isFile).length;
    return totalFile.toString() +
        " Files . " +
        totalFolder.toString() +
        " Folders";
  }
}

/*
class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}
*/
